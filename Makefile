NAME1 = ft_nm

NAME2 = ft_otool 

CFLAGS += -Wall -Wextra -Werror -I  includes -I libft/includes  #-fsanitize=address

libft = libft.a

SRC = manage_file.c manage_header.c debug.c display.c\
	  manage_load_command.c ft_binary_swap.c fat_tools.c \
	  display_load_command.c header_tools.c lib_tools.c\
	   get_args.c display_help.c manage_text_section.c search_command.c\
	  display_text_section.c display_section.c ft_putnstr.c display_segment.c\
	  display_version.c display_symtab.c display_dysymtab.c display_header.c\
	  display_linkedit.c display_unix_thread.c display_dylinker_commnd.c\
	  display_source_version.c display_lc_uuid.c display_load_dylib.c\
	  display_dyld_info_command.c display_linker_option_command.c\
	  display_sub_client_command.c display_lc_segment.c display_section_32.c\
	  display_lc_main.c display_lc_rpath.c display_sub_framework_command.c\
	  display_symbol.c manage_symbol.c manage_nlist.c nlist_trie.c\
	  manage_symbol_unsuported.c manage_symbol_lib.c\
	  manage_symbol_fat_32_little.c display_symbol_32.c init_symbol_table.c\
	  init_symbol_table_32.c search_symtab_command.c\
	  manage_load_command_lib.c manage_load_command_unsuported.c\
	  manage_load_command_fat_32_little.c display_unix_thread_x86.c\
	  display_unix_thread_i386.c manage_text_section_unsuported.c\
	  manage_text_section_lib.c manage_text_section_fat_32_little.c\
	  search_text_section_32.c nlist_trie_32.c otool_cmds.c\
	  search_text_section_64.c manage_nlist_32.c manage_header_32.c\
	  manage_header_64.c display_cara.c display_header_unsuported.c\
	  display_header_fat_32_little.c display_header_lib.c\
	  display_cara_sym_sect.c free_symbol_table.c check_size.c \
	  ft_put_file.c

SRCS = $(addprefix sources/,$(SRC))

OBJ = $(SRCS:.c=.o)

SRCSNM := sources/nm.c $(SRCS)

OBJNM = $(SRCSNM:.c=.o)

SRCTOOL := sources/otool.c  $(SRCS)

OBJTOOL = $(SRCTOOL:.c=.o)

CC = clang 

all: libft $(NAME1) $(NAME2)

libft:
	make -C libft/

$(NAME1): $(OBJNM)
	make -C libft
	$(CC) $(CFLAGS) -o $@ $^ -L ./libft -lft 

$(NAME2): $(OBJTOOL)
	make -C libft
	$(CC) $(CFLAGS) -o $@ $^ -L ./libft -lft 

clean:
	make clean -C libft/
	rm -rf sources/*.o

fclean: clean
	make fclean -C libft/
	rm -rf $(NAME1);
	rm -rf $(NAME2);


re:fclean all

#.PHONY: all clean fclean re 
.PHONY: all clean fclean re
