/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_elem.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 10:49:49 by abary             #+#    #+#             */
/*   Updated: 2017/09/15 14:55:30 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Search in the linked the data we want
** comp elem need to return 0 when the dat searched dosen't occur
** Return a pointer to the data contained in the list
*/

void	*get_elem(t_list *lst, int (*comp_elem)(void *, void *), void *search)
{
	t_elem *tmp;

	tmp = lst->begin;
	while (tmp)
	{
		if (comp_elem(tmp->data, search))
			return (tmp->data);
		tmp = tmp->next;
	}
	return (0);
}
