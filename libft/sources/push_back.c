/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_back.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 12:29:09 by abary             #+#    #+#             */
/*   Updated: 2017/09/13 16:29:32 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** add an elem at the end of the linked list
** dont allocated the value, call push_back_malloc for this
** return the new elem Created
*/

t_elem	*push_back(t_list *lst, void *data)
{
	t_elem *new;

	if (!(new = malloc(sizeof(t_elem))))
		return (0);
	new->data = data;
	new->next = 0;
	if (!lst->begin)
	{
		lst->begin = new;
		lst->end = new;
	}
	else
		lst->end->next = new;
	lst->end = new;
	lst->size++;
	return (new);
}
