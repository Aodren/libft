/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbru_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 14:58:16 by abary             #+#    #+#             */
/*   Updated: 2017/11/21 14:58:19 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_putnbrup_base(unsigned int nbr, unsigned int base)
{
	if (nbr >= base)
		ft_putnbrup_base(nbr / base, base);
	if (nbr % base < 10)
		ft_putchar('0' + nbr % base);
	else
		ft_putchar('a' + nbr % base - 10);
}

void		ft_putnbru_base(unsigned int nbr, unsigned int base)
{
	ft_putnbrup_base(nbr, base);
}
