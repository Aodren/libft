/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_front.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 13:34:18 by abary             #+#    #+#             */
/*   Updated: 2017/09/13 16:27:50 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** add an elem at the beginning  of the linked list
** dont allocated  the data
*/

t_elem	*push_front(t_list *lst, void *data)
{
	t_elem *new;

	if (!(new = malloc(sizeof(t_elem))))
		return (0);
	new->data = data;
	new->next = lst->begin;
	if (!lst->begin)
	{
		lst->begin = new;
		lst->end = new;
	}
	lst->begin = new;
	lst->size++;
	return (new);
}
