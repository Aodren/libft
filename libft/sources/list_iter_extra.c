/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_iter_extra.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 14:18:20 by abary             #+#    #+#             */
/*   Updated: 2017/09/16 14:20:01 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Same as list_iter but use an extra (void *)data with the iter function
*/

void	list_iter_extra(t_list *lst, void (*iter)(void *, void *), void *extra)
{
	t_elem *tmp;

	tmp = lst->begin;
	while (tmp)
	{
		iter(tmp->data, extra);
		tmp = tmp->next;
	}
}
