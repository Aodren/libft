/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_free.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 16:11:59 by abary             #+#    #+#             */
/*   Updated: 2017/09/13 16:29:08 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void	free_elem(t_elem *elem)
{
	if (elem)
	{
		free_elem(elem->next);
		free(elem);
	}
}

void		free_lst(t_list *lst)
{
	free_elem(lst->begin);
	free(lst);
}
