/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_iter.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 13:59:59 by abary             #+#    #+#             */
/*   Updated: 2017/09/16 14:06:34 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Call the iter function for each elem of the linked list
*/

void		list_iter(t_list *lst, void (*iter)(void *))
{
	t_elem *tmp;

	tmp = lst->begin;
	while (tmp)
	{
		iter(tmp->data);
		tmp = tmp->next;
	}
}
