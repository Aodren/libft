/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_insert_data.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 11:23:40 by abary             #+#    #+#             */
/*   Updated: 2017/09/16 13:36:33 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** Do the same thing as list_insert except the comp function has at the end
** an extra (void *) data that can be usefull for the sort.The extra data is not
** insert in any element of the linked list
*/

t_elem	*list_insert_extra(t_list *lst, void *data,
		int (*comp)(void *, void *, void *), void *extra)
{
	t_elem	*new;
	t_elem	*tmp;
	t_elem	*iter;

	if (!(new = malloc(sizeof(t_elem))))
		return (0);
	new->data = data;
	if (!(iter = lst->begin))
		return (push_back(lst, data));
	tmp = 0;
	while (iter && comp(new->data, iter->data, extra) >= 0)
	{
		tmp = iter;
		iter = iter->next;
	}
	if (!tmp && ((new->next = lst->begin)))
		lst->begin = new;
	else
	{
		tmp->next = new;
		new->next = iter;
	}
	lst->size++;
	return (new);
}
