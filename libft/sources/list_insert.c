/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_insert.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 13:50:52 by abary             #+#    #+#             */
/*   Updated: 2017/09/16 11:22:07 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** Insert a new elem in the linked list, sort by comp
*/

t_elem	*list_insert(t_list *lst, void *data, int (*comp)(void *, void *))
{
	t_elem	*new;
	t_elem	*tmp;
	t_elem	*iter;

	if (!(new = malloc(sizeof(t_elem))))
		return (0);
	new->data = data;
	if (!(iter = lst->begin))
		return (push_back(lst, data));
	tmp = 0;
	while (iter && comp(new->data, iter->data) >= 0)
	{
		tmp = iter;
		iter = iter->next;
	}
	if (!tmp && ((new->next = lst->begin)))
		lst->begin = new;
	else
	{
		tmp->next = new;
		new->next = iter;
	}
	lst->size++;
	return (new);
}
