/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abary <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/13 12:20:18 by abary             #+#    #+#             */
/*   Updated: 2017/09/13 16:27:48 by abary            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
 ** Create the header of the list
*/

t_list	*create_list(void)
{
	t_list	*lst;

	if ((lst = malloc(sizeof(t_list))))
	{
		lst->begin = 0;
		lst->end = 0;
		lst->size = 0;
	}
	return (lst);
}
